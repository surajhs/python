from ftplib import FTP
import threading
import serial
import json
def read_from_port(ser):
        isRunning=True
        try:
                while isRunning:
                        data=json.loads(ser.readline().decode('utf-8'))
                        print data["password"]
                        if(data["password"]=="1234"):
                                ser.write("U".encode('utf-8'))
                                print "Download Started"
                                ret_all_files()
                                ser.write("D".encode('utf-8'))
                        else:
                                pass
        except:
                print "Arduino not available"
def ret_all_files():
        try:
                ftp = FTP()
                ftp.set_debuglevel(2)
                ftp.connect('192.168.0.102', 21)
                ftp.login('aditya','123456')
                ftp.cwd('/upload')    # from where to upload
                gFile = open("list.txt", "wb")
                ftp.retrbinary('RETR list.txt', gFile.write)
                gFile.close()
                with open("list.txt", "r") as f:
                        content = f.readlines()
                content = [x.strip() for x in content]
                for i in content:
                        gFile = open("/media/pi/TWO/"+i, "wb")
                        ftp.retrbinary('RETR '+i, gFile.write)
                        gFile.close()
                ftp.quit()
                print "Download Successfully Completed"
        except:
                print "FTP Failure or Pendrive Not Inserted"

ser_port=serial.Serial('/dev/arduino',9600,timeout=30)
thread=threading.Thread(target=read_from_port,args=(ser_port,))
thread.start()
