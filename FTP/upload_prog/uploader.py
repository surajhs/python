from ftplib import FTP
import os
import fileinput
import threading
import time
ftp=None

def ftp_upload(localfile, remotefile):
  fp = open(localfile, 'rb')
  ftp.storbinary('STOR %s' % os.path.basename(localfile), fp, 1024)
  fp.close()
  print "after upload " + localfile + " to " + remotefile


localdir = "/media/pi/ONE/data"

def upload_img(file):
  ftp_upload(localdir + "/" + file, file)

def mythread():
        global ftp
        while(True):
                try:
                        ftp = FTP()
                        ftp.set_debuglevel(2)
                        ftp.connect('192.168.0.102', 21)
                        ftp.login('aditya','123456')
                        ftp.cwd('/upload')
                        lastlist = []
                        for line in fileinput.input(localdir + "/list.txt"):
                                lastlist.append(line.rstrip("\n"))
                        currentlist = os.listdir(localdir)
                        newfiles = list(set(currentlist) - set(lastlist))
                        if len(newfiles) == 0:
                                print "No files need to upload"
                        else:
                                for needupload in newfiles:
                                        for line in fileinput.input(localdir + "/list.txt"):
                                                lastlist.append(line.rstrip("\n"))
                                        print "uploading " + localdir + "/" + needupload
                                        upload_img(needupload)
                                        with open(localdir + "/list.txt", "a") as myfile:
                                                myfile.write(needupload + "\n")
                        upload_img("list.txt")
                        ftp.quit()
                except:
                        pass

                time.sleep(10)

t=threading.Thread(target=mythread)
t.start()
